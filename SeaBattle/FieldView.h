#pragma once
#include "IGameElement.h"
#include "Field.h"
#include "CellView.h"

namespace sea_battle {
	class FieldView : public IGameElement {
	public:
		FieldView(Field const& model, Vector2f position);
		void draw(RenderWindow& window) override;

	};
};

