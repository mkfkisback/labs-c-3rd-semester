#pragma once
#include "precomp.h"
#include "IGameElement.h"
#include "Field.h"
#include "Config.h"

namespace sea_battle {
	class CellView : public IGameElement {
		static const std::map<Cell, Color>cellColorMap;

	public:	
		CellView(Cell const& _model);
		void setPosition(float x, float y);
		void draw(RenderWindow& window) override;

	private:
		RectangleShape cell;
		Cell const& model;
	};

};

