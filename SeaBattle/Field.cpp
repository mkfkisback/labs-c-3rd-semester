#include "Field.h"

namespace sea_battle {

	Field::Field() {
		for (auto &i : _field) {
			for (auto &j : i) {
				j = Cell::EMPTY;
			}
		}
	}

	const Cell &Field::getCell(const size_t &x, const size_t &y) const {
		return _field[x][y];
	}

	void Field::setCell(const size_t &x, const size_t &y, const Cell &new_cell) {
		_field[x][y] = new_cell;
	}

	void Field::clear() {
		for (auto &i : _field) {
			for (auto &j : i) {
				j = Cell::EMPTY;
			}
		}
	}
}