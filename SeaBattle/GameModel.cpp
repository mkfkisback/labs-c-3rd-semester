#include "GameModel.h"


namespace sea_battle {
	GameModel::GameModel(std::pair<GamerMode, GamerMode>& gamersMode, RenderWindow& window):
		gamer(GamerFactory::getInstance().create(gamersMode.first, window)),
		enemy(GamerFactory::getInstance().create(gamersMode.second, window))
	{
		gameMode = GameMode::IN_PROCESS;
	}


	Field const& GameModel::getGamerField() const {
		return _gamerField;
	}

	Field const& GameModel::getEnemyField() const {
		return _enemyField;
	}

	Field const& GameModel::getEnemyAttackField() const {
		return _enemyAttackField;
	}

	Field const& GameModel::getGamerAttackField() const {
		return _gamerAttackField;
	}

	std::string GameModel::getInstruction() const {
		std::string result = gamer->getText() + instruction + EMPTY_STRING;
		return result;
	}

	void GameModel::setInstruction(std::string _instruction) {
		instruction = _instruction;
	}

	GameMode GameModel::getGameMode() const {
		return gameMode;
	}

	void GameModel::gamersFillMap() {
		gamer->fillMap(_gamerField);
		enemy->fillMap(_enemyField);
	}

	void GameModel::move() {
		static bool gamerMove = true;
		static std::pair<size_t, size_t> hitCells = std::make_pair(0, 0);
		std::pair<size_t, size_t> gameStep;
		if (gamerMove) {
			instruction = GAMER_TURN;
			
			gameStep = gamer->attack(_gamerAttackField);

			if (_enemyField.getCell(gameStep.first, gameStep.second) == Cell::SHIP) {
				++hitCells.first;
				_gamerAttackField.setCell(gameStep.first, gameStep.second, Cell::HIT);
				_enemyField.setCell(gameStep.first, gameStep.second, Cell::EMPTY);
			}
			else {
				_gamerAttackField.setCell(gameStep.first, gameStep.second, Cell::MISS);
				gamerMove = !gamerMove;
			}
			if (hitCells.first == ENOUGH_CELLS_TO_WIN) {
				gameMode = GameMode::WIN_GAMER1;
			}
		}
		else {
			instruction = ENEMY_TURN;
			
			gameStep = enemy->attack(_enemyAttackField);

			if (_gamerField.getCell(gameStep.first, gameStep.second) == Cell::SHIP) {
				++hitCells.second;
				_enemyAttackField.setCell(gameStep.first, gameStep.second, Cell::HIT);
				_gamerField.setCell(gameStep.first, gameStep.second, Cell::EMPTY);
			}
			else {
				_enemyAttackField.setCell(gameStep.first, gameStep.second, Cell::MISS);
				gamerMove = !gamerMove;
			}
			if (hitCells.second == ENOUGH_CELLS_TO_WIN) {
				gameMode = GameMode::WIN_GAMER2;
			}
		}
	}

};