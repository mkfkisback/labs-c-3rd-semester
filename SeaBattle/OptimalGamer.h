#pragma once
#include "IGamer.h"

namespace sea_battle {
	class OptimalGamer : public IGamer {
		std::pair<size_t, size_t> attack(const Field &attackField) override;
		void fillMap(Field &field) override;
	};
}