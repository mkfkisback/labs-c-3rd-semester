#pragma once

#include "IGamer.h"
#include "OptimalGamer.h"
#include "RandomGamer.h"
#include "ConsoleGamer.h"

namespace sea_battle {

	class GamerFactory {
	private:
		GamerFactory() {};

	public:
		static GamerFactory &getInstance() {
			static GamerFactory factory;
			return factory;
		}

		IGamer *create(const GamerMode &mode, RenderWindow& window) {
			switch (mode) {
			case GamerMode::CONSOLE:
				return new ConsoleGamer(window);
			case GamerMode::RANDOM:
				return new RandomGamer;
			case GamerMode::OPTIMAL:
				return new OptimalGamer;
			default:
				return nullptr;
			}
		}
	};
}