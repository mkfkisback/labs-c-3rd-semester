#pragma once
#include "precomp.h"

namespace sea_battle {

	static const std::string PROJECT_NAME = "Sea Battle";

	static const float TIME_BETWEEN_STEPS = 0.1; 

	static const float CELL_SIZE = 40;
	static const float CELL_OUTLINE_TICKNESS = 1;
	static const float FIELD_START_POSITION_X = 160;
	static const float FIELD_START_POSITION_Y = 240;
	static const float DIFFERENCE_BETWEEN_FIELDS = 200 / CELL_SIZE;
	static const float STANDART_SHIFT = CELL_SIZE / 4;

	static const size_t CELLS_COUNT = 10; // in string
	static const size_t ROWS = 10;
	static const size_t COLUMNS = 10;

	static const size_t BATTLESHIPS = 1;
	static const size_t CRUISERS = 2;
	static const size_t DESTROYERS = 3;
	static const size_t TORPEDO_BOATS = 4;

	static const size_t BATTLESHIPS_LENGTH = 3;
	static const size_t CRUISERS_LENGTH = 2;
	static const size_t DESTROYERS_LENGTH = 1;
	static const size_t TORPEDO_BOATS_LENGTH = 0;

	static const size_t INSTRUCTION_CHARACTER_SIZE = 80;
	static const size_t FIELD_LETTERS_SIZE = CELL_SIZE - 10;

	static const size_t ENOUGH_CELLS_TO_WIN = 20;

	static const size_t AMOUNT_OF_PLAYERS = 2;
	static const size_t BACKLAYER_SIZE_X = 1000;
	static const size_t BACKLAYER_SIZE_Y = 90;
	static const size_t BACKLAYER_START_POSITION_X = 0;
	static const size_t BACKLAYER_START_POSITION_Y = 100;

	static const sf::Color BACKLAYER_COLOR = sf::Color::Red;
	static const sf::Color BACKGROUND_COLOR = sf::Color::Cyan;
	static const sf::Color CELL_OUTLINE_COLOR = sf::Color::Black;
	static const sf::Color FIELD_LETTERS_COLOR = sf::Color::Black;
	static const sf::Color INSTRUCTION_LETTERS_COLOR = sf::Color::Black;
	static const sf::Text::Style TEXT_DEFAULT_STYLE = sf::Text::Style::Regular;

	static const size_t FIRST_GAMER = 1;
	static const size_t SECOND_GAMER = 2;

	static const size_t DEFAULT_WAIT_TIME = 2;
	static const size_t END_WAIT_TIME = 6;

	static const size_t FIRST_GAMER_ARRAY = 0;
	static const size_t SECOND_GAMER_ARRAY = 1;

	static const size_t ZERO_POSITION_X = 0;
	static const size_t ZERO_POSITION_Y = 0;

	static const size_t MIN_COORDINATE = 0;
	static const size_t MAX_COORDINATE = 9;

	static const size_t GAMER_TYPES = 3;

	static const char START_HORIZONTAL_SYMBOL = '0';
	static const char END_HORIZONTAL_SYMBOL = '9';
	static const char START_VERTICAL_SYMBOL = 'a';
	static const char END_VERTICAL_SYMBOL = 'j';

	static const size_t MAX_SYMBOLS_ATTACK = 2;
	static const std::string EMPTY_STRING = "";

	static const std::string ARIAL_PATH = "bin/arial.ttf";

	//phrases
	static const std::string GREETINGS = "Hello to Sea Battle!";
	static const std::string CHOSING_MODE = "Choose mode to gamer:\nPlayer as a gamer\nBot with random strategy\nBot with optimal strategy";
	static const std::string GAMER_TURN = "Gamer, your turn(write coords) :";
	static const std::string ENEMY_TURN = "Enemy, your turn(write coords) :";
	static const std::string GAMER_WON = "Gamer won the game!!";
	static const std::string ENEMY_WON = "Enemy won the game!!";
	static const std::string WRONG_COORDINATES = "Wrong coordinates, try again";
	static const std::string SET_SHIPS_WITH = "Set ships with length = ";

}
