#include "GameView.h"

namespace sea_battle {
	GameView::GameView(GameModel const& _model) : model(_model) {
		Vector2f gamerFieldPosition(FIELD_START_POSITION_X, FIELD_START_POSITION_Y);
		Vector2f enemyFieldPosition(FIELD_START_POSITION_X + (CELLS_COUNT + DIFFERENCE_BETWEEN_FIELDS)*CELL_SIZE, FIELD_START_POSITION_Y);

		FieldView* _gamerAttackFieldView = new FieldView(_model.getGamerAttackField(), gamerFieldPosition);
		FieldView* _enemyAttackFieldView = new FieldView(_model.getEnemyAttackField(), enemyFieldPosition);

		this->addChild(*_gamerAttackFieldView);
		this->addChild(*_enemyAttackFieldView);
	}

	void GameView::draw(RenderWindow& window) {
		std::string instruction = model.getInstruction();
		Text text;
		Font font;
		if (font.loadFromFile(ARIAL_PATH)) {
			text.setFont(font);
		}
		text.setCharacterSize(INSTRUCTION_CHARACTER_SIZE);
		text.setFillColor(INSTRUCTION_LETTERS_COLOR);
		text.setStyle(TEXT_DEFAULT_STYLE);
		text.setPosition(ZERO_POSITION_X, ZERO_POSITION_Y);
		text.setString(instruction);
		window.draw(text);
	}

};