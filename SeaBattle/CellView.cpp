#include "CellView.h"

namespace sea_battle {
	const std::map<Cell, Color> CellView::cellColorMap = {
		{Cell::EMPTY,Color::White},
		{Cell::SHIP, Color::Blue},
		{Cell::HIT, Color::Red},
		{Cell::MISS,Color::Yellow}
	};

	CellView::CellView(Cell const& _model) : model(_model) {};

	void CellView::setPosition(float x, float y) {
		setPos(x, y);
		cell.setPosition(getPos().x,getPos().y);
	}

	void CellView::draw(RenderWindow& window) {
		RectangleShape _cell(Vector2f(CELL_SIZE, CELL_SIZE));
		_cell.setFillColor(cellColorMap.at(model));
		_cell.setOutlineThickness(CELL_OUTLINE_TICKNESS);
		_cell.setOutlineColor(CELL_OUTLINE_COLOR);
		_cell.setPosition(cell.getPosition());
		cell = _cell;
		window.draw(cell);
	}

};