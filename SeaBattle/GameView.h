#pragma once
#include "IGameElement.h"
#include "FieldView.h"
#include "GameModel.h"

namespace sea_battle {
	class GameView : public IGameElement {
	public:
		GameView(GameModel const& _model);
		void draw(RenderWindow& window) override;

	private:
		GameModel const& model;

	};
};

