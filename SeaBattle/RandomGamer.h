#pragma once

#include "IGamer.h"
#include <vector>

namespace sea_battle {

	class RandomGamer : public IGamer {
	public:
		std::pair<size_t, size_t> attack(const Field &attackField) override;
		void fillMap(Field &field) override;
	private:
	};

}