#pragma once

#include "Field.h"
#include "GameController.h"

namespace sea_battle {

	class IGameView {
	public:
		virtual void drawGameState(const Field&, const Field&) = 0;
		virtual void printInstruction(const std::string&) = 0;
		virtual void drawField(const Field &field) = 0;
	};
}