#pragma once
#include "Config.h"

namespace sea_battle {

	enum class Cell {
		EMPTY = 0, SHIP, MISS, HIT
	};

	class Field {
	private:
		Cell _field[ROWS][COLUMNS]{};
	public:
		Field();
		const Cell &getCell(const size_t &x, const size_t &y) const;
		void setCell(const size_t &x, const size_t &y, const Cell &new_cell);
		void clear();
	};

}