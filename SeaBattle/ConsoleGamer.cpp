#pragma once
#include "ConsoleGamer.h"
#include "Config.h"

namespace sea_battle {

	ConsoleGamer::ConsoleGamer(RenderWindow& _window) : window(_window) {};

	bool checkCoords(std::string str, const Field field, int shipLength) {
		int x0 = str[0] - START_VERTICAL_SYMBOL;
		int x1 = str[2] - START_VERTICAL_SYMBOL;
		int y0 = str[1] - START_HORIZONTAL_SYMBOL;
		int y1 = str[3] - START_HORIZONTAL_SYMBOL;
		if (x0 < MIN_COORDINATE || y0 < MIN_COORDINATE || x1 < MIN_COORDINATE || y1 < MIN_COORDINATE || x0 > MAX_COORDINATE || y0 > MAX_COORDINATE || x1 > MAX_COORDINATE || y1 > MAX_COORDINATE)return false;
		if (x0 != x1 && y0 != y1)return false;
		if (abs(y1 - y0) != shipLength && abs(x1 - x0) != shipLength)return false;
		for (int i = std::min(x0, x1) - 1; i <= std::max(x0, x1) + 1; i++) {
			for (int j = std::min(y0, y1) - 1; j <= std::max(y0, y1) + 1; j++) {
				if (i >= MIN_COORDINATE && i <= MAX_COORDINATE && j >= MIN_COORDINATE && j <= MAX_COORDINATE && field.getCell(i, j) != Cell::EMPTY)return false;
			}
		}
		return true;
	}

	bool checkAttack(std::string str, const Field field) {
		if (str[0]<START_VERTICAL_SYMBOL || str[0]>END_VERTICAL_SYMBOL)return false;
		if (str[1]<START_HORIZONTAL_SYMBOL || str[1]>END_HORIZONTAL_SYMBOL)return false;
		if (field.getCell(str[0] - START_VERTICAL_SYMBOL, str[1] - START_HORIZONTAL_SYMBOL) == Cell::MISS || field.getCell(str[0] - START_VERTICAL_SYMBOL, str[1] - START_HORIZONTAL_SYMBOL) == Cell::HIT)return false;
		return true;
	}

	bool checkSymbol(const char sym) {
		if (sym >= START_VERTICAL_SYMBOL && sym <= END_VERTICAL_SYMBOL)return true;
		if (sym >= START_HORIZONTAL_SYMBOL && sym <= END_HORIZONTAL_SYMBOL)return true;
		return false;
	}

	std::pair<size_t, size_t> ConsoleGamer::attack(const Field &attackField) {
		Event event;
		while (true) {
			int symbols = 0;
			std::string enteredString;
			while (symbols < MAX_SYMBOLS_ATTACK) {
				while (window.pollEvent(event)) {
					if (event.type == Event::TextEntered) {
						if (event.text.unicode < 128) {
							if (!checkSymbol(static_cast<char>(event.text.unicode))) {
								continue;
							}
							symbols++;
							enteredString += static_cast<char>(event.text.unicode);
						}
					}
				}
			}
			if (!checkAttack(enteredString, attackField)) {
				continue;
			}

			return std::make_pair(enteredString[0] - START_VERTICAL_SYMBOL, enteredString[1] - START_HORIZONTAL_SYMBOL);
		}
	}

	void ConsoleGamer::setShips(int amountShips, int shipLength, Field &field) {
		while (amountShips > 0) {			
			text.setString(SET_SHIPS_WITH + std::to_string(shipLength + 1) + " mister (write coords from/to)");
			Event event;
			int symbols = 0;
			std::string enteredString;
			while (symbols < 4) {
				while (window.pollEvent(event)) {
					if (event.type == Event::TextEntered) {
						if (event.text.unicode < 128) {
							if (!checkSymbol(static_cast<char>(event.text.unicode))) {
								continue;
							}
							symbols++;
							enteredString += static_cast<char>(event.text.unicode);
							text.setString("Your coords: " + enteredString);
						}
					}
				}
			}
			if (!checkCoords(enteredString, field, shipLength)) {
				text.setString(WRONG_COORDINATES);
				sleep(seconds(1));
				continue;
			}
			amountShips--;
			std::string from = enteredString.substr(0, 2);
			std::string to = enteredString.substr(2, 2);
			for (int i = std::min(from[0], to[0]) - START_VERTICAL_SYMBOL; i <= std::max(from[0], to[0]) - START_VERTICAL_SYMBOL; i++) {
				for (int j = std::min(from[1], to[1]) - START_HORIZONTAL_SYMBOL; j <= std::max(from[1], to[1]) - START_HORIZONTAL_SYMBOL; j++) {
					field.setCell(i, j, Cell::SHIP);
				}
			}
		}
	}

	std::string ConsoleGamer::getText() {
		return text.getString();
	}

	void ConsoleGamer::setText(Text& _text) {
		if (font.loadFromFile(ARIAL_PATH)) {
			_text.setFont(font);
		}
		_text.setCharacterSize(INSTRUCTION_CHARACTER_SIZE);
		_text.setFillColor(INSTRUCTION_LETTERS_COLOR);
		_text.setStyle(TEXT_DEFAULT_STYLE);
	}

	void ConsoleGamer::fillMap(Field &field) {
		setText(text);

		setShips(BATTLESHIPS, BATTLESHIPS_LENGTH, field);
		setShips(CRUISERS, CRUISERS_LENGTH, field);
		setShips(DESTROYERS, DESTROYERS_LENGTH, field);
		setShips(TORPEDO_BOATS, TORPEDO_BOATS_LENGTH, field);
	}
}