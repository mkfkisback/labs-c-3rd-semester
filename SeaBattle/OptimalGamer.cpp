#pragma once
#include "OptimalGamer.h"
#include <algorithm>

namespace sea_battle {

	bool checkCoordinates(int x0, int y0, int x1, int y1, Field field) {
		if (x0 < 0 || y0 < 0 || x1 < 0 || y1 < 0 || x0 > 9 || y0 > 9 || x1 > 9 || y1 > 9) return false;
		for (int i = std::min(x0, x1) - 1; i <= std::max(x0, x1) + 1; i++) {
			for (int j = std::min(y0, y1) - 1; j <= std::max(y0, y1) + 1; j++) {
				if (i >= 0 && i < 10 && j >= 0 && j < 10 && field.getCell(i, j) != Cell::EMPTY) return false;
			}
		}
		return true;
	}

	int inverse(int a) {
		return 1 - a;
	}

	void putAngle(Field &field, int *angleShips, int shipLength) {
		int angle;
		int x0, y0, x1, y1;
		int dir = rand() % 2;
		while (true) {
			angle = rand() % 4;
			if (angleShips[angle] == 0) break;
		}
		angleShips[angle] = 1;
		switch (angle) {
		case 0:
			x0 = y0 = 0;
			x1 = x0 + shipLength * dir;
			y1 = y0 + shipLength * inverse(dir);
			break;
		case 1:
			x0 = 9;
			y0 = 0;
			x1 = x0 - shipLength * dir;
			y1 = y0 + shipLength * inverse(dir);
			break;
		case 2:
			x0 = y0 = 9;
			x1 = x0 -shipLength*dir;
			y1 = y0 -shipLength*inverse(dir);
			break;
		case 3:
			x0 = 0;
			y0 = 9;
			x1 = x0 + shipLength*dir;
			y1 = y0 - shipLength*inverse(dir);
			break;
		default:
			break;
		}
		for (int j = 0; j < shipLength + 1; ++j) {
			if (x0 == x1) {
				int direction = y1 - y0;
				field.setCell(x0, direction > 0 ? y0 + j : y0 - j, Cell::SHIP);
			}
			if (y0 == y1) {
				int direction = x1 - x0;
				field.setCell(direction > 0 ? x0 + j : x0 - j, y0, Cell::SHIP);
			}
		}
	}

	void putSides(Field &field) {
		int x0, y0, x1, y1;
		int count = 0;

		x0 = x1 = 0;
		for (y0 = 0; y0 < 9; y0++) {
			y1 = y0 + 1;
			if (checkCoordinates(x0, y0, x1, y1, field)) {
				for (int j = 0; j < 2; ++j) {
					if (x0 == x1) {
						int direction = y1 - y0;
						field.setCell(x0, direction > 0 ? y0 + j : y0 - j, Cell::SHIP);
					}
					if (y0 == y1) {
						int direction = x1 - x0;
						field.setCell(direction > 0 ? x0 + j : x0 - j, y0, Cell::SHIP);
					}
				}
				count++;
				break;
			}
		}
		y0 = y1 = 9;
		for (x0 = 0; x0 < 9; x0++) {
			x1 = x0 + 1;
			if (checkCoordinates(x0, y0, x1, y1, field)) {
				for (int j = 0; j < 2; ++j) {
					if (x0 == x1) {
						int direction = y1 - y0;
						field.setCell(x0, direction > 0 ? y0 + j : y0 - j, Cell::SHIP);
					}

					if (y0 == y1) {
						int direction = x1 - x0;
						field.setCell(direction > 0 ? x0 + j : x0 - j, y0, Cell::SHIP);
					}
				}
				count++;
				break;
			}
		}
		if (count < 2) {
			x0 = x1 = 9;
			for (y0 = 9; y0 > 0; y0--) {
				y1 = y0 - 1;
				if (checkCoordinates(x0, y0, x1, y1, field)) {
					for (int j = 0; j < 2; ++j) {
						if (x0 == x1) {
							int direction = y1 - y0;
							field.setCell(x0, direction > 0 ? y0 + j : y0 - j, Cell::SHIP);
						}

						if (y0 == y1) {
							int direction = x1 - x0;
							field.setCell(direction > 0 ? x0 + j : x0 - j, y0, Cell::SHIP);
						}
					}
					count++;
					break;
				}
			}
		}
		if (count < 2) {
			y0 = y1 = 0;
			for (x0 = 9; x0 > 0; x0--) {
				x1 = x0 - 1;
				if (checkCoordinates(x0, y0, x1, y1, field)) {
					for (int j = 0; j < 2; ++j) {
						if (x0 == x1) {
							int direction = y1 - y0;
							field.setCell(x0, direction > 0 ? y0 + j : y0 - j, Cell::SHIP);
						}
						if (y0 == y1) {
							int direction = x1 - x0;
							field.setCell(direction > 0 ? x0 + j : x0 - j, y0, Cell::SHIP);
						}
					}
					break;
				}
			}
		}
	}

	void putSingles(Field &field) {
		int x0, y0;
		for (int i = 0; i < 4; i++) {
			while (true) {
				x0 = rand() % 10;
				y0 = rand() % 10;
				if (field.getCell(x0, y0) == Cell::EMPTY && checkCoordinates(x0, y0, x0, y0, field)) break;
			}
			field.setCell(x0, y0, Cell::SHIP);
		}
	}

	std::pair<size_t, size_t> OptimalGamer::attack(const Field &attackField) {
		size_t x, y, step;
		while (true) {
			x = rand() % 10;
			y = rand() % 10;

			if (attackField.getCell(x, y) == Cell::EMPTY) {
				return std::make_pair(x, y);
			}

			if (attackField.getCell(x, y) == Cell::HIT) {
				if (((x > 1) && attackField.getCell(x - 1, y) == Cell::HIT) || ((x < 8) && attackField.getCell(x + 1, y) == Cell::HIT)) {
					if ((x > 2) && attackField.getCell(x - 2, y) == Cell::EMPTY) {
						return std::make_pair(x - 2, y);
					}

					if ((x < 7) && attackField.getCell(x + 2, y) == Cell::EMPTY) {
						return std::make_pair(x + 2, y);
					}

					continue;
				}

				if (((y > 1) && attackField.getCell(x, y - 1) == Cell::HIT) || ((y < 8) && attackField.getCell(x, y + 1) == Cell::HIT)) {
					if ((y > 2) && attackField.getCell(x, y - 2) == Cell::EMPTY) {
						return std::make_pair(x, y - 2);
					}

					if ((y < 7) && attackField.getCell(x, y + 2) == Cell::EMPTY) {
						return std::make_pair(x, y + 2);
					}
					continue;
				}
			}
		
		}
	}

	void OptimalGamer::fillMap(Field &field) {
		int angleShips[4] = { 0 };

		putAngle(field, angleShips, BATTLESHIPS_LENGTH);
		putAngle(field, angleShips, CRUISERS_LENGTH);
		putAngle(field, angleShips, CRUISERS_LENGTH);
		putAngle(field, angleShips, DESTROYERS_LENGTH);

		putSides(field);

		putSingles(field);
	}
}