#pragma once
#include "IGameController.h"
#include "precomp.h"

namespace sea_battle {

	class GameController : public IGameController {

	public:
		void run() override;
	};

}