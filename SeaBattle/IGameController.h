#pragma once
#include "IGamer.h"
namespace sea_battle {

	class IGameController {
		virtual void run() = 0;
	};
}