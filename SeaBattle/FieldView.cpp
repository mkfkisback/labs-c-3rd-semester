#include "FieldView.h"

namespace sea_battle {

	FieldView::FieldView(Field const& model, Vector2f position) {
		this->setPos(position.x, position.y);
		for (int i = 0; i < COLUMNS; i++) {
			for (int j = 0; j < ROWS; j++) {
				auto cell = new CellView(model.getCell(i, j));
				cell->setPosition(this->getPos().x + CELL_SIZE * i, this->getPos().y + CELL_SIZE * j);
				this->addChild(*cell);
			}
		}
	}

	void FieldView::draw(RenderWindow& window) {
		Text message;
		Font font;
		if (font.loadFromFile(ARIAL_PATH)) {
			message.setFont(font);
		}
		message.setCharacterSize(FIELD_LETTERS_SIZE);
		message.setFillColor(FIELD_LETTERS_COLOR);
		message.setStyle(TEXT_DEFAULT_STYLE);
		for (int i = 0; i < COLUMNS; i++) {
			const char tmp = START_HORIZONTAL_SYMBOL + i;
			message.setPosition(this->getPos().x + CELL_SIZE * i + STANDART_SHIFT, this->getPos().y - CELL_SIZE);
			message.setString(tmp);
			window.draw(message);
		}
		for (int i = 0; i < ROWS; i++) {
			const char tmp = START_VERTICAL_SYMBOL + i;
			message.setPosition(this->getPos().x - CELL_SIZE + STANDART_SHIFT, this->getPos().y + CELL_SIZE * i);
			message.setString(tmp);
			window.draw(message);
		}
	}

};