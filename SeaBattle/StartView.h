#pragma once
#include "IGameElement.h"
#include "precomp.h"
#include <functional>
#include "IGamer.h"

namespace sea_battle {
	class StartView : public IGameElement {
	public:
		void setText(Text& _text);
		void draw(RenderWindow& window) override;
		void run(RenderWindow& window);
		const std::map<Keyboard::Key, std::function<void(bool&, size_t&)>>actionsWhenKeyPressed = {
							{Keyboard::Up, [this](bool& typed, size_t& gamerType) {
								gamerType--;
								gamerType = (gamerType + GAMER_TYPES) % GAMER_TYPES;
								backlayer.setPosition(BACKLAYER_START_POSITION_X, BACKLAYER_START_POSITION_Y + gamerType * BACKLAYER_SIZE_Y);
								 }},

							{Keyboard::Down, [&](bool& typed, size_t& gamerType) {
								gamerType++;
								gamerType = gamerType % GAMER_TYPES;
								backlayer.setPosition(BACKLAYER_START_POSITION_X, BACKLAYER_START_POSITION_Y + gamerType * BACKLAYER_SIZE_Y);
								 }},

							{Keyboard::Enter, [&](bool& typed, size_t& gamerType) {
								typed = true;
						 }} };
		std::pair<GamerMode, GamerMode> getGamersMode();
	private:
		Font font;
		Text instruction;
		RectangleShape backlayer;
		GamerMode gamerMode1, gamerMode2;
	};
};

