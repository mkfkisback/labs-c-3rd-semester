#pragma once

#include <iostream>
#include "Field.h"

namespace sea_battle {
	enum class GamerMode {
		CONSOLE = 0, RANDOM = 1, OPTIMAL = 2
	};

	class IGamer {
	public:
		virtual void fillMap(Field &map) = 0;
		virtual std::pair<size_t, size_t> attack(const Field &attackMap) = 0;
		virtual ~IGamer() {};
		virtual std::string getText() {
			return EMPTY_STRING;
		}
	};
}