#pragma once
#include "IGamer.h"
#include "precomp.h"

namespace sea_battle {
	class ConsoleGamer : public IGamer {
	public:
		explicit ConsoleGamer(RenderWindow& _window);
		std::pair<size_t, size_t> attack(const Field &attackField) override;
		void fillMap(Field &field) override;
		void setShips(int amountShips, int shipLength, Field &field);
		void setText(Text& _text);
		std::string getText() override;

	private:
		Text text;
		Font font;
		RenderWindow& window;
	};

}