#include "StartView.h"


namespace sea_battle {
	void StartView::setText(Text& _text) {
		if (font.loadFromFile(ARIAL_PATH)) {
			_text.setFont(font);
		}
		_text.setCharacterSize(INSTRUCTION_CHARACTER_SIZE);
		_text.setFillColor(INSTRUCTION_LETTERS_COLOR);
		_text.setStyle(TEXT_DEFAULT_STYLE);
	}

	void StartView::run(RenderWindow& window) {
		// Greetings
		setText(instruction);
		instruction.setPosition(ZERO_POSITION_X, ZERO_POSITION_Y);
		instruction.setString(GREETINGS);
		window.clear(BACKGROUND_COLOR);
		draw(window);
		sleep(seconds(DEFAULT_WAIT_TIME));

		// Gamer Choice
		GamerMode gamerMode[AMOUNT_OF_PLAYERS];
		for (int i = 0; i < AMOUNT_OF_PLAYERS; i++) {
			instruction.setString(CHOSING_MODE);
			backlayer.setSize(Vector2f(BACKLAYER_SIZE_X, BACKLAYER_SIZE_Y));
			backlayer.setFillColor(BACKLAYER_COLOR);
			backlayer.setPosition(BACKLAYER_START_POSITION_X, BACKLAYER_START_POSITION_Y);
			window.clear(BACKGROUND_COLOR);
			draw(window);

			bool typed = false;
			size_t gamerType = 0;			

			while (!typed) {
				Event event;
				while (window.pollEvent(event) && !typed) {
					if (event.type == Event::KeyPressed) {
						actionsWhenKeyPressed.at(event.key.code)(typed, gamerType);
						gamerMode[i] = (GamerMode)gamerType;
						window.clear(BACKGROUND_COLOR);
						draw(window);
					}
				}
			}
			window.clear(BACKGROUND_COLOR);
			window.display();
			sleep(seconds(DEFAULT_WAIT_TIME));
		}
		gamerMode1 = gamerMode[FIRST_GAMER_ARRAY];
		gamerMode2 = gamerMode[SECOND_GAMER_ARRAY];
	}

	std::pair<GamerMode, GamerMode> StartView::getGamersMode() {
		return { gamerMode1,gamerMode2 };
	}

	void StartView::draw(RenderWindow& window) {
		window.draw(backlayer);
		window.draw(instruction);
		window.display();
	}

};
