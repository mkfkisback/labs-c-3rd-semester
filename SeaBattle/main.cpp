#include "precomp.h"
#include "GameController.h"
#include "GameView.h"

using namespace sea_battle;

void main(int argc, char **argv) {
	if (argc > 1) {
		std::cout << R"(please use pattern "SeaBattle.exe")";
		return;
	}

	GameController game;
	game.run();
}