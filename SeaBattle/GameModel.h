#pragma once
#include "Field.h"
#include "IGamer.h"
#include "GamerFactory.h"

namespace sea_battle {
	enum class GameMode {
		IN_PROCESS, WIN_GAMER1, WIN_GAMER2
	};

	class GameModel {
	public:
		GameModel(std::pair<GamerMode, GamerMode>& gamersMode, RenderWindow& window);


		Field const& getGamerField() const;

		Field const& getEnemyField() const;

		Field const& getEnemyAttackField() const;

		Field const& getGamerAttackField() const;

		std::string getInstruction() const;

		void setInstruction(std::string _instruction);

		GameMode getGameMode() const;

		void gamersFillMap();

		void move();

	private:
		std::unique_ptr<IGamer> gamer, enemy;
		Field _gamerField, _gamerAttackField;
		Field _enemyField, _enemyAttackField;
		GameMode gameMode;
		std::string instruction;
	};
};