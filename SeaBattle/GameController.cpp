#include "GameController.h"
#include <ctime>
#include "GamerFactory.h"
#include <chrono>
#include <thread>
#include "GameView.h"
#include "GameModel.h"
#include "StartView.h"

namespace sea_battle {
	using namespace std::this_thread;
	using namespace std::chrono_literals;
	using std::chrono::system_clock;

	void printResult(GameModel& model) {
		if (model.getGameMode() == GameMode::WIN_GAMER1){
			model.setInstruction(GAMER_WON);
		}
		else {
			model.setInstruction(ENEMY_WON);
		}
	}

	void GameController::run() {
		srand(time(nullptr));
		RenderWindow window(VideoMode::getDesktopMode(), PROJECT_NAME);
		window.setFramerateLimit(30);
		while (window.isOpen()) {
			Event event;
			while (window.pollEvent(event)) {
				if (event.type == Event::Closed) {
					window.close();
				}
			}
			StartView startView;
			startView.run(window);

			GamerMode gamerMode1, gamerMode2;
			auto gamersMode = startView.getGamersMode();

			gamerMode1 = gamersMode.first;
			gamerMode2 = gamersMode.second;

			GameModel gameModel(gamersMode,window);
			GameView gameView(gameModel);

			window.clear(BACKGROUND_COLOR);
			gameView.drawAll(window);
			window.display();
			sleep(seconds(TIME_BETWEEN_STEPS));
			gameModel.gamersFillMap();
			while (gameModel.getGameMode() == GameMode::IN_PROCESS) {
				gameModel.move();
				window.clear(BACKGROUND_COLOR);
				gameView.drawAll(window);
				window.display();		
				sleep(seconds(TIME_BETWEEN_STEPS));
			}
			window.clear(BACKGROUND_COLOR);
			printResult(gameModel);
			gameView.drawAll(window);
			window.display();
			sleep(seconds(END_WAIT_TIME));
			break;
		}
	}
}