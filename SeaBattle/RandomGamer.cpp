#include "RandomGamer.h"
#include <vector>
#include <algorithm>
#include <random>

namespace sea_battle {

	bool checkShipCoordinates(int x0, int y0, int x1, int y1, Field field) {
		if (x0 < 0 || y0 < 0 || x1 < 0 || y1 < 0 || x0 > 9 || y0 > 9 || x1 > 9 || y1 > 9)return false;
		for (int i = std::min(x0, x1) - 1; i <= std::max(x0, x1) + 1; i++) {
			for (int j = std::min(y0, y1) - 1; j <= std::max(y0, y1) + 1; j++) {
				if (i >= 0 && i <= 9 && j >= 0 && j <= 9 && field.getCell(i, j) != Cell::EMPTY)return false;
			}
		}
		return true;
	}

	void shipRandomSet(int amountShips, int shipLength, Field &field) {
		int dir;
		int x0, y0, x1, y1;

		for (int i = 0; i < amountShips; i++) {
			while (true) {
				x0 = rand() % CELLS_COUNT;
				y0 = rand() % CELLS_COUNT;
				dir = rand() % 4;

				if (field.getCell(x0, y0) == Cell::EMPTY) {
					if (dir == 0 && checkShipCoordinates(x0, y0, x0, y0 - shipLength, field)) {
						y1 = y0 - shipLength;
						x1 = x0;
						break;
					}
					if (dir == 1 && checkShipCoordinates(x0, y0, x0, y0 + shipLength, field)) {
						y1 = y0 + shipLength;
						x1 = x0;
						break;
					}
					if (dir == 2 && checkShipCoordinates(x0, y0, x0 - shipLength, y0, field)) {
						y1 = y0;
						x1 = x0 - shipLength;
						break;
					}
					if (dir == 3 && checkShipCoordinates(x0, y0, x0 + shipLength, y0, field)) {
						y1 = y0;
						x1 = x0 + shipLength;
						break;
					}
				}
			}

			for (int j = 0; j < shipLength + 1; j++) {
				if (x0 == x1) {
					int direction = y1 - y0;
					field.setCell(x0, direction > 0 ? y0 + j : y0 - j, Cell::SHIP);
				}
				if (y0 == y1) {
					int direction = x1 - x0;
					field.setCell(direction > 0 ? x0 + j : x0 - j, y0, Cell::SHIP);
				}
			}
		}
	}


	std::pair<size_t, size_t> RandomGamer::attack(const Field &attackField) {
		size_t x, y;
		do {
			x = rand() % CELLS_COUNT;
			y = rand() % CELLS_COUNT;
		} while (attackField.getCell(x, y) != Cell::EMPTY);
		return std::make_pair(x, y);
	}

	void RandomGamer::fillMap(Field &field) {
		shipRandomSet(BATTLESHIPS, BATTLESHIPS_LENGTH, field);
		shipRandomSet(CRUISERS, CRUISERS_LENGTH, field);
		shipRandomSet(DESTROYERS, DESTROYERS_LENGTH, field);
		shipRandomSet(TORPEDO_BOATS, TORPEDO_BOATS_LENGTH, field);
	}

}