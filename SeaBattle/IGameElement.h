#pragma once
#include "precomp.h"

class IGameElement {
public:
	IGameElement() {};
	virtual ~IGameElement() {};

	void drawAll(RenderWindow& window) {
		this->draw(window);

		for (int i = 0; i < _childs.size(); ++i) {
			_childs[i]->drawAll(window);
		}
	}

	void addChild(IGameElement& child) {
		_childs.push_back(&child);
	}

	Vector2f getPos() {
		return position;
	}

	void setPos(float x, float y) {
		Vector2f _position(x, y);
		position = _position;
	}

protected:
	virtual void draw(RenderWindow& window) = 0;

private:
	std::vector<IGameElement*> _childs;

	Vector2f position;

};

