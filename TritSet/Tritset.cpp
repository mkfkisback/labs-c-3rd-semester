#include <iostream>
#include <cmath>
#include <algorithm>
#include "Tritset.h"
#include "Trit.h"

namespace maksakovsky {
	Tritset::Tritset(int _size) {
		vec.assign(ceil((double)_size / (4 * sizeof(uint))), 0);
		size = _size;
		first_size = vec.size();
		trueCount = 0;
		falseCount = 0;
	}

	Tritset::Tritset() {
		size = 0;
		first_size = 0;
		trueCount = 0;
		falseCount = 0;
	}

	Tritset::~Tritset() {
		std::vector<uint>().swap(vec);
	}

	size_t Tritset::getCapacity() const {
		return vec.capacity();
	}

	size_t Tritset::getSize() const {
		return size;
	}

	size_t Tritset::getVecSize() const {
		return vec.size();
	}

	Trit Tritset::getValue(int index) const {
		if (index >= size) {
			return Unknown;
		}
		int pos1 = index / (4 * sizeof(uint));
		int pos2 = index % (4 * sizeof(uint));

		uint tmp = vec[pos1];
		tmp >>= 2 * pos2;
		tmp &= 3;
		switch (tmp) {
		case False:
			return False;
		case True:
			return True;
		default:
			return Unknown;
		}
	}

	uint Tritset::at(int index) const {
		if (index >= vec.size()) {
			return 0;
		}

		return vec[index];
	}

	void Tritset::shrink() {
		size_t new_size = first_size;
		for (size_t k = vec.size() - 1; k > first_size; k--) {
			if (vec[k] > 0) {
				new_size = k + 1;
				break;
			}
		}
		vec.resize(new_size);

		int first_i = 4 * sizeof(uint)*(new_size - 1);
		int last_i = 4 * sizeof(uint)*new_size;
		for (int i = first_i; i < last_i; i++) {
			if (!(getValue(i) == Unknown)) {
				size = i + 1;
			}
		}
		vec.shrink_to_fit();
	}

	size_t Tritset::cardinality(Trit value) const {
		switch (value) {
		case True:
			return trueCount;
		case False:
			return falseCount;
		default:
			return (size - trueCount - falseCount);
		}
	}

	std::unordered_map <Trit, int, std::hash<int>> Tritset::cardinality() {
		std::unordered_map <Trit, int, std::hash<int>> res;
		res[True] = cardinality(True);
		res[False] = cardinality(False);
		res[Unknown] = cardinality(Unknown);
		return res;
	}

	void Tritset::trim(size_t lastIndex) {
		if (lastIndex >= size) {
			return;
		}
		int new_size = ceil((double)lastIndex / (4 * sizeof(uint)));
		int pos1 = new_size * 4 * sizeof(uint);

		for (int i = lastIndex; i < pos1; i++) {
			setValue(i, Unknown);
		}
		vec.resize(new_size);
		size = lastIndex + 1;
		vec.shrink_to_fit();
	}

	size_t Tritset::length() {
		int last_ind = -1;
		for (int i = getVecSize() - 1; i >= 0; i--) {
			if (vec[i] != 0) {
				last_ind = i;
				break;
			}
		}
		if (last_ind == -1) {
			return 0;
		}

		int first_i = 4 * sizeof(uint) * last_ind;
		int last_i = 4 * sizeof(uint) * (last_ind + 1);

		for (int i = last_i + 1; i >= first_i; --i) {
			if (getValue(i) != Unknown) {
				return i + 1;
			}
		}
		return 0;
	}

	void Tritset::setValue(int index, Trit val) {
		switch (getValue(index)) {
		case True:
			trueCount--;
			break;
		case False:
			falseCount--;
			break;
		default:
			break;
		}

		switch (val) {
		case True:
			trueCount++;
			break;
		case False:
			falseCount++;
			break;
		default:
			break;
		}

		if (val == Unknown && index >= size) {
			return;
		}

		int pos1 = index / (4 * sizeof(uint));
		int pos2 = index % (4 * sizeof(uint));

		uint tmp1, tmp2;
		tmp1 = 3;
		switch (val) {
		case False:
			tmp2 = False;
			break;
		case True:
			tmp2 = True;
			break;
		default:
			tmp2 = Unknown;
			break;
		}
		tmp1 <<= 2 * pos2;
		tmp2 <<= 2 * pos2;
		tmp1 = ~tmp1;
		vec[pos1] &= tmp1;
		vec[pos1] |= tmp2;
	}

	Tritset::ProxyTritset::ProxyTritset(Tritset *_set, int _index) : set(*_set), index(_index) {}

	Tritset::ProxyTritset &Tritset::ProxyTritset::operator=(Trit value) {
		if (set.vec.size() < ceil((double)(index + 1) / (4 * sizeof(uint))) && value != Unknown) {
			set.vec.resize(ceil((double)(index + 1) / (4 * sizeof(uint))), 0);
			set.size = index + 1;
		}
		else if (index >= set.size && value != Unknown) {
			set.size = index + 1;
		}

		set.setValue(index, value);
		return *this;
	}

	bool Tritset::ProxyTritset::operator==(const Tritset::ProxyTritset &pTritset) const {
		if (index < set.size && pTritset.index < pTritset.set.size) {
			return (set.getValue(index) == pTritset.set.getValue(pTritset.index));
		}
		else if (index >= set.size && pTritset.set.getValue(pTritset.index) == Unknown) {
			return true;
		}
		else if (pTritset.index >= pTritset.set.size && set.getValue(index) == Unknown) {
			return true;
		}
		return false;
	}

	Tritset::ProxyTritset::operator Trit() {
		return this->set.getValue(this->index);
	}

	bool Tritset::ProxyTritset::operator==(const Trit &trit) const {
		if (index > set.size) {
			return trit == Unknown;
		}
		return (set.getValue(index) == trit);
	}

	Tritset::ProxyTritset Tritset::operator[] (int index) {
		return ProxyTritset(this, index);
	}

	Tritset& Tritset::operator=(const Tritset &tritset) {
		vec = tritset.vec;
		size = tritset.size;
		trueCount = tritset.trueCount;
		falseCount = tritset.falseCount;
		return *this;
	}

	Tritset operator~ (const Tritset &tritset) {
		Tritset res(tritset.getSize());

		for (int i = 0; i < tritset.getVecSize(); ++i) {
			if (tritset.at(i) == 0) continue;

			int first_i = i * 4 * sizeof(uint);
			int last_i = (i + 1) * 4 * sizeof(uint);

			for (int j = first_i; j < last_i; ++j) {
				res[j] = ~tritset.getValue(j);
			}
		}
		return res;
	}

	Tritset operator& (const Tritset &tritset1, const Tritset &tritset2) {
		Tritset res(std::max(tritset1.getSize(), tritset2.getSize()));
		for (int i = 0; i < std::max(tritset1.getVecSize(), tritset2.getVecSize()); ++i) {
			if (tritset1.at(i) == 0 && tritset2.at(i) == 0) {
				continue;
			}

			int first_i = i * 4 * sizeof(uint);
			int last_i = (i + 1) * 4 * sizeof(uint);

			for (int j = first_i; j < last_i; ++j) {
				res[j] = tritset1.getValue(j) & tritset2.getValue(j);
			}
		}
		return res;
	}

	Tritset operator| (const Tritset &tritset1, const Tritset &tritset2) {
		Tritset res(std::max(tritset1.getSize(), tritset2.getSize()));
		for (int i = 0; i < std::max(tritset1.getVecSize(), tritset2.getVecSize()); ++i) {
			if (tritset1.at(i) == 0 && tritset2.at(i) == 0) {
				continue;
			}

			int first_i = i * 4 * sizeof(uint);
			int last_i = (i + 1) * 4 * sizeof(uint);

			for (int j = first_i; j < last_i; ++j) {
				res[j] = tritset1.getValue(j) | tritset2.getValue(j);
			}
		}
		return res;
	}
}