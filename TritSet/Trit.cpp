#include "Trit.h"

namespace maksakovsky {
	Trit operator&(Trit left, Trit right) {
		if (left == Trit::True && right == Trit::True)return Trit::True;
		if (right == Trit::False || left == Trit::False) return Trit::False;
		return Trit::Unknown;
	}

	Trit operator|(Trit left, Trit right) {
		if (left == Trit::True || right == Trit::True) return Trit::True;
		if (left == Trit::False && right == Trit::False) return Trit::False;
		return Trit::Unknown;
	}

	Trit operator~(Trit Trit) {
		if (Trit == Trit::False) return Trit::True;
		if (Trit == Trit::True) return Trit::False;
		return Trit::Unknown;
	}
}