#pragma once

namespace maksakovsky {
	enum Trit {
		False = 2, Unknown = 0, True = 3
	};

	Trit operator&(Trit left, Trit right);

	Trit operator|(Trit left, Trit right);

	Trit operator~(Trit trit);
}