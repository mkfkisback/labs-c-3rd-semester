#include <vector>
#include <unordered_map>
#include "Trit.h"

namespace maksakovsky {

	using uint = unsigned int;
	class Tritset {
	public:
		/*������������*/
		explicit Tritset();
		explicit Tritset(int _size);

		/*����������*/
		virtual ~Tritset();

		/*���������� ����������� �������*/
		size_t getCapacity() const;

		/*���������� ������ ���� � ������*/
		size_t getSize() const;

		/*���������� ������ ������� � uint'��*/
		size_t getVecSize() const;

		/*���������� �������� ����� � ���� �� �������*/
		Trit getValue(int index) const;

		/*���������� �������� uint'� � ������� �� �������*/
		uint at(int index) const;

		/*����������� ������ �� ��������������� ������� ��� �� ���������� �������������� �����*/
		void shrink();

		/*���������� ���������� ������ ������������� ���� � ����*/
		size_t cardinality(Trit val) const;

		/*���������� ���������� ������ ������� ���� � ����*/
		std::unordered_map <Trit, int, std::hash<int>> cardinality();

		/*�������� ���������� ����, ������� � lastIndex, ����������� ������*/
		void trim(size_t lastIndex);

		/*���������� ������ ���������� �� Unknown ����� + 1*/
		size_t length();

	private:
		/*������-�����, ����������� ��� ���������� ���������� ��� ��������� �� �������*/
		class ProxyTritset {
			friend class Tritset;
		public:
			Tritset &set;
			int index;
			ProxyTritset(Tritset *_set, int _index);

			/*�������� ������������ �� �������*/
			ProxyTritset &operator=(Trit value);

			/*��������� ���������*/
			bool operator==(const ProxyTritset &pTritset) const;
			bool operator==(const Trit &trit) const;

			/*�������� �������� ���������� ������-������ � ���� Trit*/
			operator Trit();
		};

	public:
		/*�������� ��������� �� �������*/
		ProxyTritset operator[](int index);

		/*�������� ������������ ����� ����������*/
		Tritset &operator=(const Tritset &tritset);

	private:
		/*������ uint*/
		std::vector <uint> vec;

		/*������ �������������� ������ ���� ��� ������� shrink*/
		size_t first_size;

		/*������ ������ ���� � ������*/
		size_t size;

		/*������ ���������� ������ True � ����*/
		size_t trueCount;

		/*������ ���������� ������ False � ����*/
		size_t falseCount;

		/*������������� �������� � ���� �� �������*/
		void setValue(int index, Trit val);
	};

	/*���������� "��" ��� ����� ����*/
	Tritset operator~(const Tritset &tritset);

	/*���������� "�" ��� ���� �����*/
	Tritset operator&(const Tritset &tritset1, const Tritset &tritset2);

	/*���������� "���" ��� ���� �����*/
	Tritset operator|(const Tritset &tritset1, const Tritset &tritset2);

}