#include "Writer.h"
namespace maksakovsky {
    Writer::Writer(const std::string &fileName) {
        this->output.open(fileName);
    }

    void Writer::write(WordCounter &counter) {
        std::vector<std::pair<int, std::string>> word_pairs;

        for (const auto &pair : counter.getFreqs()) {
            word_pairs.emplace_back(pair.second, pair.first);
        }
        std::sort(word_pairs.rbegin(), word_pairs.rend());
        for (const auto &pair : word_pairs) {
            output << std::to_string(pair.first) + SEPARATOR + pair.second + SEPARATOR +
                      std::to_string((long double) 100. * pair.first / counter.getSize()) + ENDLINE;
        }
    }
}