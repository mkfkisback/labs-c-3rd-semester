#include "Reader.h"

namespace maksakovsky {
    Reader::Reader(const std::string &fileName) {
        this->input.open(fileName);
    }


    void Reader::read(WordCounter &counter) {
        std::string line;
        while (std::getline(this->input, line)) {
            counter.count(line);
        }
    }
}