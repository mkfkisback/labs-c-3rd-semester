#pragma once
#include <iostream>
#include <string>
#include <map>
namespace maksakovsky {
    class WordCounter {
    public:
        WordCounter();
        virtual ~WordCounter();

        void count(std::string &line);

        unsigned long long getSize() const;

        std::map<std::string, int> getFreqs();

    private:
        std::map<std::string, int> frequencies;
        unsigned long long size;
    };
}

