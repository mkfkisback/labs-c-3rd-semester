#pragma once
#include <fstream>
#include <string>
#include <iostream>
#include "WordCounter.h"

namespace maksakovsky {
    class Reader {
    public:
        Reader(const std::string &fileName);

        void read(WordCounter &counter);

    private:
        std::ifstream input;
    };
}
