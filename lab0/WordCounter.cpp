#include "WordCounter.h"
#include <vector>
#include <algorithm>

namespace maksakovsky {
    WordCounter::WordCounter() {
        size = 0;
    }

    WordCounter::~WordCounter() {
        frequencies.clear();
    }

    bool isLetter(char c) {
        return ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z'));
    }

    void WordCounter::count(std::string &line) {
        std::string word;
        for (size_t i = 0; i < line.length(); i++) {
            line[i] = tolower(line[i]);
            while (isLetter(line[i])) {
                word += line[i];
                i++;
            }
            if (!word.empty()) {
                frequencies[word]++;
                size++;
                word.clear();
            }
        }
    }

    unsigned long long WordCounter::getSize() const {
        return size;
    }

    std::map<std::string, int> WordCounter::getFreqs() {
        return this->frequencies;
    }
}