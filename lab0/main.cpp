#include <iostream>
#include "Reader.h"
#include "WordCounter.h"
#include "Writer.h"

using namespace maksakovsky;

int main(int argc, char **argv) {
    const int MIN_COUNT_OF_ARGUMENTS = 3;
    const int INPUT_ARGUMENT = 1;
    const int OUTPUT_ARGUMENT = 2;

    if (argc < MIN_COUNT_OF_ARGUMENTS) {
        std::cout << R"(please use pattern "program 'input.txt' 'output.csv'")";
        return 0;
    }

    Reader reader(argv[INPUT_ARGUMENT]);
    Writer writer(argv[OUTPUT_ARGUMENT]);
    WordCounter counter;
    reader.read(counter);
    writer.write(counter);
}


