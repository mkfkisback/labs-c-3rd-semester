#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <map>
#include "WordCounter.h"

namespace {
    const char ENDLINE = '\n';
    const char SEPARATOR = ';';
}
namespace maksakovsky {
    class Writer {
    public:
        Writer(const std::string &fileName);

        void write(WordCounter &counter);

    private:
        std::ofstream output;
    };

}
